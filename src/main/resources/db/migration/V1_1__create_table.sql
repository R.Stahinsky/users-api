CREATE TYPE user_role AS ENUM ('ROLE_USER', 'ROLE_ADMIN');
CREATE TABLE IF NOT EXISTS public.users
(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    first_name text,
    last_name text,
    phone text,
    email text NOT NULL UNIQUE,
    password text NOT NULL,
    created_at timestamp DEFAULT NOW() NOT NULL,
    updated_at timestamp,
    deleted_at timestamp,
    role user_role NOT NULL
);
ALTER TABLE public.users
    OWNER to postgres;
