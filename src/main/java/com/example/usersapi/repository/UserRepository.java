package com.example.usersapi.repository;

import com.example.usersapi.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    boolean existsByEmail(final String email);

    @Query(value = "SELECT EXISTS(SELECT * FROM users WHERE id = :id AND deleted_at IS NULL)", nativeQuery = true)
    boolean existsById(@Param("id") final UUID id);

    @Modifying
    @Query(value = "UPDATE users SET deleted_at = NOW() WHERE id = :id ;", nativeQuery = true)
    void deleteById(@Param("id") final UUID id);

    @Query(value = "SELECT * FROM users WHERE id = :id AND (:isAdmin = true OR deleted_at IS NULL);",
                    nativeQuery = true)
    Optional<UserEntity> findById(@Param("id") final UUID id, @Param("isAdmin") final boolean isAdmin);

    Optional<UserEntity> findByEmail(final String email);

    @Query(value = "SELECT * FROM users WHERE :isAdmin = true OR deleted_at IS NULL", nativeQuery = true)
    Page<UserEntity> findAll(final Pageable pageable, @Param("isAdmin") final boolean isAdmin);
}
