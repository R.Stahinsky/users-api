package com.example.usersapi.model.update;

import com.example.usersapi.utils.validation.annotations.Phone;
import lombok.Data;

@Data
public class UserUpdateRequest {

    private String firstName;
    private String lastName;

    @Phone
    private String phone;
}
