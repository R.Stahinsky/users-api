package com.example.usersapi.model.update;

import com.example.usersapi.utils.validation.annotations.Password;
import lombok.Data;

@Data
public class PasswordUpdateRequest {

    @Password
    private String oldPassword;

    @Password
    private String newPassword;
}
