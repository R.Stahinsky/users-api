package com.example.usersapi.model.update;

import com.example.usersapi.utils.validation.annotations.Email;
import lombok.Data;

@Data
public class EmailUpdateRequest {

    @Email
    private String oldEmail;

    @Email
    private String newEmail;
}
