package com.example.usersapi.model.auth;

import com.example.usersapi.utils.validation.annotations.Email;
import com.example.usersapi.utils.validation.annotations.Password;
import com.example.usersapi.utils.validation.annotations.Phone;
import lombok.Data;

@Data
public class SignUpRequest {

    @Email
    private String email;

    @Password
    private String password;

    @Phone
    private String phone;

    private String firstName;
    private String lastName;
}
