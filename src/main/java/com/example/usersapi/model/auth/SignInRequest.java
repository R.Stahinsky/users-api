package com.example.usersapi.model.auth;

import lombok.Data;

@Data
public class SignInRequest {

    private String email;
    private String password;
}
