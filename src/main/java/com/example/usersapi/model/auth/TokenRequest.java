package com.example.usersapi.model.auth;

import lombok.Data;

@Data
public class TokenRequest {

    private String token;
}
