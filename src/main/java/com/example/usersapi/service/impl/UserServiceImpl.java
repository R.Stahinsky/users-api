package com.example.usersapi.service.impl;

import com.example.usersapi.entity.UserEntity;
import com.example.usersapi.enums.UserEntityRole;
import com.example.usersapi.enums.UserRole;
import com.example.usersapi.exceptions.*;
import com.example.usersapi.model.*;
import com.example.usersapi.model.auth.SignUpRequest;
import com.example.usersapi.model.update.EmailUpdateRequest;
import com.example.usersapi.model.update.PasswordUpdateRequest;
import com.example.usersapi.model.update.UserUpdateRequest;
import com.example.usersapi.repository.UserRepository;
import com.example.usersapi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.UUID;

import static com.example.usersapi.enums.UserRole.ROLE_ADMIN;
import static com.example.usersapi.mapper.UserMapper.USER_MAPPER;
import static com.example.usersapi.utils.PagingUtils.toPageRequest;
import static com.example.usersapi.utils.SecurityUtils.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public User create(final SignUpRequest requestUser) {

        final UserEntity userToSave = UserEntity.builder()
                .password(passwordEncoder.encode(requestUser.getPassword()))
                .email(requestUser.getEmail())
                .firstName(requestUser.getFirstName())
                .lastName(requestUser.getLastName())
                .phone(requestUser.getPhone())
                .createdAt(Instant.now())
                .role(UserEntityRole.ROLE_USER)
                .build();

        return USER_MAPPER.toModel(userRepository.saveAndFlush(userToSave));
    }

    @Override
    @Transactional(readOnly = true)
    public boolean existsByEmail(final String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public User getByEmail(final String email) {
        return USER_MAPPER.toModel(
                userRepository.findByEmail(email).orElseThrow(
                        () -> new EntityNotFoundException(String.format("User with %s is not found", email))));
    }

    @Override
    public Page<User> getAll(final int page, final int size) {
        return userRepository.findAll(toPageRequest(page, size), isAdmin()).map(USER_MAPPER::toModel);
    }

    @Override
    public User getById(final UUID id) {
        return USER_MAPPER.toModel(
                userRepository.findById(id, isAdmin()).orElseThrow(
                        () -> new EntityNotFoundException(String.format("User with %s is not found", id))));
    }

    @Override
    @Transactional
    public User updateById(final UUID id, final UserUpdateRequest userRequest) {
        checkAccess(id);
        final UserEntity userToUpdate = USER_MAPPER.toEntity(getById(id));

        userToUpdate.setFirstName(userRequest.getFirstName());
        userToUpdate.setLastName(userRequest.getLastName());
        userToUpdate.setPhone(userRequest.getPhone());

        return USER_MAPPER.toModel(userRepository.saveAndFlush(userToUpdate));
    }

    @Override
    @Transactional
    public void changeEmail(final EmailUpdateRequest emailRequest) {
        final User currentUser = getCurrent();

        if (!existsByEmail(emailRequest.getNewEmail())) {
            currentUser.setEmail(emailRequest.getNewEmail());

            userRepository.save(USER_MAPPER.toEntity(currentUser));
        } else {
            throw new EntityAlreadyProcessedException(String.format("Email: %s already exist", emailRequest.getNewEmail()));
        }
    }

    @Override
    @Transactional
    public void changePassword(final PasswordUpdateRequest passwordRequest) {
        final User currentUser = getCurrent();

        checkPassword(passwordRequest.getOldPassword(), currentUser.getPassword());

        currentUser.setPassword(passwordEncoder.encode(passwordRequest.getNewPassword()));

        userRepository.saveAndFlush(USER_MAPPER.toEntity(currentUser));

    }

    @Override
    @Transactional
    public void deleteById(final UUID id) {
        checkAccess(id);
        if (existById(id)) {
            userRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException(String.format("User with %s is not found", id));
        }
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetailsImpl loadUserByUsername(final String email) throws UsernameNotFoundException {
        final UserEntity foundUser = userRepository.findByEmail(email).orElseThrow(() -> new UnauthorizedException("Email or password is invalid"));

        final UserDetailsImpl userDetails = new UserDetailsImpl();
        userDetails.setEmail(foundUser.getEmail());
        userDetails.setPassword(foundUser.getPassword());
        userDetails.setRole(foundUser.getRole());

        return userDetails;
    }

    private User checkAccess(final UUID id) {
        final User currentUser = getCurrent();
        if (!(currentUser.getId().equals(id) || currentUser.getRole().equals(ROLE_ADMIN))) {
            throw new AccessDeniedException(String.format("Low access to update user with id: %s", id));
        }
        return currentUser;
    }

    private void checkPassword(final String requestedPassword, final String existedPassword) {
        if (!passwordEncoder.matches(requestedPassword, existedPassword)) {
            throw new BadRequestException("Passwords don`t matches");
        }
    }

    private User getCurrent() {
        return USER_MAPPER.toModel(
                userRepository.findByEmail(getCurrentUser().getEmail()).orElseThrow(
                        () -> new EntityNotFoundException(
                                String.format("Not logged in account with email: %s", getCurrent().getEmail()))));
    }

    private boolean existById(final UUID id) {
        return userRepository.existsById(id);
    }
}
