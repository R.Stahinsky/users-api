package com.example.usersapi.service.impl;

import com.example.usersapi.exceptions.UnauthorizedException;
import com.example.usersapi.exceptions.EntityAlreadyProcessedException;
import com.example.usersapi.model.*;
import com.example.usersapi.config.JwtTokenProvider;
import com.example.usersapi.model.auth.SignInRequest;
import com.example.usersapi.model.auth.SignUpRequest;
import com.example.usersapi.model.auth.TokenRequest;
import com.example.usersapi.model.auth.TokenResponse;
import com.example.usersapi.service.AuthService;
import com.example.usersapi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserService userService;

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public User signUp(final SignUpRequest requestUser) {
        if (userService.existsByEmail(requestUser.getEmail())) {
            throw new EntityAlreadyProcessedException(String.format("User with %s already exist", requestUser.getEmail()));
        }
        return userService.create(requestUser);
    }

    @Override
    public TokenResponse signIn(final SignInRequest requestUser) {
        final Authentication authentication =
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(requestUser.getEmail(), requestUser.getPassword()));
        if (authentication == null) {
            throw new UnauthorizedException("Email or password is invalid");
        }
        return jwtTokenProvider.createToken(authentication);
    }

    @Override
    public TokenResponse refreshToken(final TokenRequest tokenRequest) {
        return jwtTokenProvider.refreshToken(tokenRequest.getToken());
    }
}
