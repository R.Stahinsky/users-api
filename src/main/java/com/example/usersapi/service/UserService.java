package com.example.usersapi.service;

import com.example.usersapi.model.update.EmailUpdateRequest;
import com.example.usersapi.model.update.PasswordUpdateRequest;
import com.example.usersapi.model.update.UserUpdateRequest;
import com.example.usersapi.model.auth.SignUpRequest;
import com.example.usersapi.model.User;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.UUID;


public interface UserService extends UserDetailsService {

    User create(final SignUpRequest requestUser);

    boolean existsByEmail(final String email);

    User getByEmail(final String email);

    Page<User> getAll(final int page, final int size);

    User getById(final UUID id);

    User updateById(final UUID id, final UserUpdateRequest userUpdateRequest);

    void changeEmail(final EmailUpdateRequest emailRequest);

    void changePassword(final PasswordUpdateRequest passwordRequest);

    void deleteById(final UUID id);
}
