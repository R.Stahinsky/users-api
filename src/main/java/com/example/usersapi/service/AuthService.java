package com.example.usersapi.service;

import com.example.usersapi.model.*;
import com.example.usersapi.model.auth.SignInRequest;
import com.example.usersapi.model.auth.SignUpRequest;
import com.example.usersapi.model.auth.TokenRequest;
import com.example.usersapi.model.auth.TokenResponse;

public interface AuthService {

    User signUp(final SignUpRequest requestUser);

    TokenResponse signIn(final SignInRequest requestUser);

    TokenResponse refreshToken(final TokenRequest tokenRequest);
}
