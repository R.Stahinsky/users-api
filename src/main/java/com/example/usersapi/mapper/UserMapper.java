package com.example.usersapi.mapper;

import com.example.usersapi.entity.UserEntity;
import com.example.usersapi.model.auth.SignUpRequest;
import com.example.usersapi.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    UserEntity toEntity(final User user);

    User toModel(final UserEntity userEntity);

    User toModel(final SignUpRequest requestUser);
}
