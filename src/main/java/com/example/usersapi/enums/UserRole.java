package com.example.usersapi.enums;

public enum UserRole {
    ROLE_USER, ROLE_ADMIN
}
