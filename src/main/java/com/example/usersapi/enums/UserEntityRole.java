package com.example.usersapi.enums;

public enum UserEntityRole {
    ROLE_USER, ROLE_ADMIN
}
