package com.example.usersapi.controller;

import com.example.usersapi.model.*;
import com.example.usersapi.model.auth.SignInRequest;
import com.example.usersapi.model.auth.SignUpRequest;
import com.example.usersapi.model.auth.TokenRequest;
import com.example.usersapi.model.auth.TokenResponse;
import com.example.usersapi.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthController {

    private final AuthService authService;

    @PostMapping("/sign-up")
    public ResponseEntity<User> signUp(@Valid @RequestBody final SignUpRequest requestUser) {
        return new ResponseEntity<>(authService.signUp(requestUser), HttpStatus.CREATED);
    }

    @PostMapping("/sign-in")
    public ResponseEntity<TokenResponse> signIn(@Valid @RequestBody final SignInRequest requestUser) {
        return new ResponseEntity<>(authService.signIn(requestUser), HttpStatus.CREATED);
    }

    @PostMapping("/refresh")
    public ResponseEntity<TokenResponse> refresh(@RequestBody final TokenRequest tokenRequest) {
        return new ResponseEntity<>(authService.refreshToken(tokenRequest), HttpStatus.CREATED);
    }
}
