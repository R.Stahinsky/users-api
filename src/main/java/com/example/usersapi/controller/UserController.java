package com.example.usersapi.controller;

import com.example.usersapi.config.SwaggerConfig;
import com.example.usersapi.model.update.EmailUpdateRequest;
import com.example.usersapi.model.update.PasswordUpdateRequest;
import com.example.usersapi.model.User;
import com.example.usersapi.model.update.UserUpdateRequest;
import com.example.usersapi.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static com.example.usersapi.utils.HeaderUtils.generateHeaders;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;

    @GetMapping
    @ApiOperation(authorizations = @Authorization(value = SwaggerConfig.AUTH), value = "getAll")
    public ResponseEntity<List<User>> getAll(@RequestParam(defaultValue = "1") final int page, @RequestParam(defaultValue = "10") final int size) {
        final Page<User> foundUsers = userService.getAll(page, size);
        final MultiValueMap<String, String> headers = generateHeaders(foundUsers);

        return new ResponseEntity<>(foundUsers.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(authorizations = @Authorization(value = SwaggerConfig.AUTH), value = "getById")
    public ResponseEntity<User> getById(@PathVariable final UUID id) {
        return new ResponseEntity<>(userService.getById(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @ApiOperation(authorizations = @Authorization(value = SwaggerConfig.AUTH), value = "updateById")
    public ResponseEntity<User> updateById(@PathVariable final UUID id, @Valid @RequestBody final UserUpdateRequest userUpdateRequest) {
        return new ResponseEntity<>(userService.updateById(id, userUpdateRequest), HttpStatus.OK);
    }

    @PutMapping("/change-email")
    @ApiOperation(authorizations = @Authorization(value = SwaggerConfig.AUTH), value = "changeEmail")
    public ResponseEntity<Void> changeEmail(@Valid @RequestBody final EmailUpdateRequest emailUpdateRequest) {
        userService.changeEmail(emailUpdateRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/change-password")
    @ApiOperation(authorizations = @Authorization(value = SwaggerConfig.AUTH), value = "changePassword")
    public ResponseEntity<Void> changePassword(@Valid @RequestBody final PasswordUpdateRequest passwordUpdateRequest) {
        userService.changePassword(passwordUpdateRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(authorizations = @Authorization(value = SwaggerConfig.AUTH), value = "deleteByID")
    public ResponseEntity<Void> deleteById(@PathVariable final UUID id) {
        userService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
