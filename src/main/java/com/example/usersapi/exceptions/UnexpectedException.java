package com.example.usersapi.exceptions;

import lombok.Data;

@Data
public class UnexpectedException {

    private String message;
}
