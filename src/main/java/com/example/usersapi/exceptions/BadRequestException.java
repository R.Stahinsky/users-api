package com.example.usersapi.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public class BadRequestException extends RuntimeException {
    @Getter
    private final HttpStatus status;

    public BadRequestException(final String message) {
        super(message);
        this.status = HttpStatus.BAD_REQUEST;
    }
}
