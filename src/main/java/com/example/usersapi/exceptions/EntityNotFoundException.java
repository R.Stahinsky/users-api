package com.example.usersapi.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public class EntityNotFoundException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public EntityNotFoundException(final String message) {
        super(message);
        this.status = HttpStatus.NOT_FOUND;
    }
}
