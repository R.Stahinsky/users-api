package com.example.usersapi.utils.validation;

import com.example.usersapi.utils.validation.annotations.Phone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneValidator implements ConstraintValidator<Phone, String> {

    private static final Pattern PHONE_PATTERN = Pattern.compile("^(\\+\\d{12})");

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        final Matcher matcher = PHONE_PATTERN.matcher(value);
        return matcher.matches();
    }
}
