package com.example.usersapi.utils.validation.constants;

public class Messages {

    public static final String INVALID_EMAIL_MESSAGE = "Not valid email. Must be similar to: example@gmail.com";
    public static final String INVALID_PASSWORD_MESSAGE =
            "Not valid password. Password should be at least 6 characters long and should have both letters and numbers";
    public static final String INVALID_PHONE_MESSAGE = "Not valid phone number. Must be similar to: +375_________";
}
