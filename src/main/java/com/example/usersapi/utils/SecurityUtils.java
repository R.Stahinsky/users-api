package com.example.usersapi.utils;

import com.example.usersapi.enums.UserEntityRole;
import com.example.usersapi.model.UserDetailsImpl;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

@UtilityClass
public class SecurityUtils {

    public static UserDetailsImpl getCurrentUser(){
        final Object auth = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (auth instanceof UserDetails){
            return (UserDetailsImpl) auth;
        }
        return null;
    }

    public static boolean isAdmin() {
        return (getCurrentUser() != null && getCurrentUser().getRole().equals(UserEntityRole.ROLE_ADMIN));
    }
}
